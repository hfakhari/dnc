import importlib
import inspect

from celery import Celery
from flask import Flask, session, g, request, jsonify, abort, url_for
from flask_login import LoginManager, login_user
from flask_restful import Api

from mongoengine import connect

from symcrypt import celeryconfig
from symcrypt.user import send_reset_pass_email
from symcrypt.user.models import UserModel
from symcrypt.conf import MONGO_HOST, MONGO_PORT, SECRET_KEY, INSTALLED_MODULES, BROKER_URL

connect('symcrypt', host=MONGO_HOST, port=MONGO_PORT)


app = Flask(__name__)
app.secret_key = SECRET_KEY

login_manager = LoginManager()
login_manager.init_app(app)

api = Api(app)


app.config.update(BROKER_URL=BROKER_URL)


def make_celery(app):
    # create context tasks in celery
    celery = Celery(
        app.import_name,
        broker=app.config['BROKER_URL'],
        include=['symcrypt.price']
    )
    celery.conf.update(app.config)
    celery.config_from_object(celeryconfig)
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask

    return celery


celery = make_celery(app)


@login_manager.user_loader
def load_user(user):
    return UserModel.objects(username=user).first()


@app.route('/user/login', methods=['POST'])
def login():
    u = UserModel.objects(username=request.form['username']).first()
    print(u)
    for user in UserModel.objects:
        print(user)
    if u:
        if u.password == request.form['password']:
            login_user(u) 
            return jsonify(
                {
                    'err': '',
                }
            ), 200
        else:
            return jsonify(
                {
                    'err': 'username or password is not correct',
                }
            ), 400
    else:
        return jsonify(
            {
                'err': 'the user does not exist'
            }
        ), 404


@app.route('/user/forget', methods=['POST'])
def forget_password():
    u = UserModel.objects(email=request.form['email']).first()
    if u:
        send_reset_pass_email(u)
        return jsonify({
            'message': 'an email sent for you to reset the password'
        }), 200
    return jsonify({
        'err': 'User with provided email address not found'
    }), 404


for pkg in INSTALLED_MODULES:
    service_script = importlib.import_module(
        "symcrypt.%s.resources" % pkg)
    admin_script = importlib.import_module('symcrypt.%s.models' % pkg)
    for name, obj in inspect.getmembers(service_script):
        if inspect.isclass(obj) and 'symcrypt.%s' % pkg in str(obj) and 'Resource' in str(obj):
            api.add_resource(
                obj,
                '/%s/%s' % (pkg, obj.url),
                endpoint='%s.%s' % (pkg, obj.__name__)
            )
    for name, obj in inspect.getmembers(admin_script):
        if inspect.isclass(obj) and 'api.%s' % pkg in str(obj) and 'Model' in str(obj):
            admin.add_view(ModelView(obj))
