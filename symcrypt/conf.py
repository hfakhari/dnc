# Database config
MONGO_HOST = 'localhost'
MONGO_PORT = 27017

# API Config
SECRET_KEY = 'Something Secret'

# Modules
INSTALLED_MODULES = [
    'user',
    'crypto',
    'price'
]

REDIS_HOST = "0.0.0.0"
REDIS_PORT = 6379

BROKER_URL = "redis://{host}:{port}/0".format(host=REDIS_HOST, port=str(REDIS_PORT))
CELERY_RESULT_BACKEND = BROKER_URL

BITCOIN_API_URL = 'https://api.coindesk.com/v1/bpi/currentprice.json'
