from mongoengine import Document, StringField, FloatField


class PriceModel(Document):
    time = StringField()
    bpi_usd = FloatField()
    bpi_gbp = FloatField()
    bpi_eur = FloatField()
