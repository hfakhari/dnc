import requests

from symcrypt import celery


@celery.task()
def get_price():
    data = requests.get('https://api.coindesk.com/v1/bpi/currentprice.json')
    if data.status_code == 200:
        data.json()
