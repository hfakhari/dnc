from uuid import uuid4
from datetime import datetime
from .models import ForgetPass


def send_reset_pass_email(user):
    created = datetime.now()
    nonce = uuid4()
    fp = ForgetPass.objects(user=user, nonce=nonce, created=created, used=False)
    # sending email with fp info
    print('sending resset password to user')
