import requests
import unittest


class TestLogin(unittest.TestCase):
    '''
    Before running this test module please create following user using
    MongoDB console:
        + username: vahid
        + password: test
    '''

    def test_login_success(self):
        r = requests.post(
            'http://localhost:4000/user/login',
            data={
                'username': 'vahid',
                'password': 'test'
            }
        )
        self.assertEqual('', r.json()['err'])

    def test_login_wrong_username(self):
        r = requests.post(
            'http://localhost:4000/user/login',
            data={
                'username': 'majid',
                'password': 'test'
            }
        )
        self.assertEqual(404, r.status_code)

    def test_login_wrong_password(self):
        r = requests.post(
            'http://localhost:4000/user/login',
            data={
                'username': 'vahid',
                'password': 'wrong password'
            }
        )
        self.assertEqual(400, r.status_code)


class TestForgetPass(unittest.TestCase):
    def test_forget_pass_invalid_email(self):
        r = requests.post(
            'http://localhost:4000/user/forget',
            data={
                'email': 'someone@somewhere.com'
            }
        )
        self.assertEqual(404, r.status_code)

    def test_forget_pass_success(self):
        r = requests.post(
            'http://localhost:4000/user/forget',
            data={
                'email': 'vahid@dnc.com'
            }
        )
        self.assertEqual(200, r.status_code)


if __name__ == '__main__':
    unittest.main()
